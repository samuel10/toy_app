class ApplicationController < ActionController::Base
  def hello
  	render text: "Esto es Toy_App"
  end

  protect_from_forgery with: :exception
end
